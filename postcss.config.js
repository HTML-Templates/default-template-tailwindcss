module.exports = {
    plugins: [
        require('tailwindcss'),
        require('cssnano')({
            preset: 'default',
        }),
        process.env.NODE_ENV === 'build-production' && require('@fullhuman/postcss-purgecss')({
            content: ['/public/**/*.html'],
            defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
        }),
        require('autoprefixer'),
    ]
}