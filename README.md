# Setting up Tailwind and PostCSS - cssnano & Purgecss

1. [Install & Setting up - Tailwind and PostCSS](https://tailwindcss.com/course/setting-up-tailwind-and-postcss/)
2. [Install - cssnano for CSS compression](https://cssnano.co/)
3. [Install - purgecss to Optimizing css for Production](https://www.purgecss.com/)

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. npm init -y
2. npm install tailwindcss postcss-cli autoprefixer

Then, generate a `tailwind.config.js` file:

3. npx tailwind init

Next, create a `postcss.config.js` file:

```
module.exports = {
    plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
    ]
}
```

Now create a CSS file { build/style.css } and add the special @tailwind directives:

```
@tailwind base;
@tailwind components;
@tailwind utilities;
```

Add a simple build script to your `package.json` file to compile your CSS:

```
{
    // ...
    "scripts": {
        "build": "postcss build/style.css -o public/css/myapp.min.css"
    },
    // ...
}
```

Run your build script to generate your CSS:

```
npm run build
```

Create a simple HTML file that includes your compiled CSS:

```
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/css/myapp.min.css">
</head>

<body>
    <h1 class="text-4xl font-bold text-center text-blue-500">Hello world!</h1>
</body>

</html>
```
#

## Installation of cssnano

```
npm install cssnano
```

Once you have done this, you will need to configure cssnano by creating a `postcss.config.js` file in the root of your project

```
module.exports = {
    plugins: [
        require('tailwindcss'),
        //------------------------------------------
        //...... Paste it Just in a next Line ......
        //------------------------------------------
        require('cssnano')({
            preset: 'default',
        }),

        //.....
    ],
};
```

#

## Installation of purgecss

```
npm install @fullhuman/postcss-purgecss
```

Then we add this to our PostCSS configuration file `postcss.config.js`:

```
module.exports = {
    plugins: [
        //.....

        process.env.NODE_ENV === 'build-production' && require('@fullhuman/postcss-purgecss')({
            content: ['./layouts/**/*.html'],
            defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
        }),
        //----------------------------------------------------
        //...... Paste it Just before the Last Line ......
        //----------------------------------------------------
        require('autoprefixer'),
    ]
}
```

`content: ['./layouts/**/*.html'] => Path to Root Template or Html file folder`


For Non Laravel Template Development Please install cross-env

```
npm install cross-env --save-dev
```

To Compile Optimizing CSS System Please Add in `package.json` file

```
"scripts": {
    "build-production": "cross-env NODE_ENV=build-production postcss public/build/style.css -o public/css/myapp.min.css"
},
```

